# Coffee Cup Game

A Coffee Cup Game thats better than drinking Coffee in real lyfe

## Features

- dynamic setting of number of cups
- high fidelity 4k HDR graphics
- optimized for smartphones
- offline caching
- personal highscore functionality

## Usage

### Building

1. run `npm i`

2. To start the development server with hot module reloading, run:

    * `npm start`
    
    To build for production
    
    * first remove the folders `dist` and `.cache`
    * then run: `npm run build`
    
    *Note, parcel is currently unable to support minfication os ES2015 code—github.com/parcel-bundler/parcel/issues/8*
