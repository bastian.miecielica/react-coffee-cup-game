import React from "react";
import ReactDOM from "react-dom";

import { SettingsScreen } from "./screens/settings"
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { DescriptionScreen } from "./screens/description";
import { GameScreen } from "./screens/game";
import { Header } from "./components/styling";




const App = () => (



  <Router>
    <div>
      <Header>
        <img src={require("./images/cup_too_full.png")} />Coffe Game
       
      </Header>
      <Switch>
        <Route path="/settings" component={SettingsScreen} />
        <Route path="/game" component={GameScreen} />
        <Route path="/description" component={DescriptionScreen} />
        <Route path="/home" component={SettingsScreen} />
        <Route path="/" component={SettingsScreen} />
      </Switch>
    </div>
  </Router>

);

ReactDOM.render(<App />, document.getElementById("root"));


module.hot.accept();
if ('serviceWorker' in navigator) {
  window.addEventListener('load', function () {
    navigator.serviceWorker.register('service-worker.js').then(function (registration) {
      // Registration was successful
      console.log('ServiceWorker registration successful with scope: ', registration.scope);
    }, function (err) {
      // registration failed :(
      console.log('ServiceWorker registration failed: ', err);
    }).catch(function (err) {
      console.log(err)
    });
  });
} else {
  console.log('service worker is not supported');
}

// Hot Module Replacement
if (module.hot) {
  //dev mode
 
} else {

  

  // production mode
}
