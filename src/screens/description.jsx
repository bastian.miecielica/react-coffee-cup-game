import React from "react";

export class DescriptionScreen extends React.Component {

    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(e) {
        const show = e.target.value
        this.props.showDescription(show)
    }
    render() {
        return (
            <div>
                <h1>Test Description</h1>
                <input type="checkbox" onClick={this.handleClick} />
                <label>Don't show this message again</label>
            </div>
        )
    }
}