import React from "react";
import { CupSelector } from "../components/cupSelector";
import { Link } from "react-router-dom";
import { Description } from "../components/description";
import { Row, CoffeeLink, CoffeeDescripton } from "../components/styling";


export class SettingsScreen extends React.Component {


    constructor(props) {
        super(props);
        const skip = (localStorage.getItem("skipDescription") === 'true')
        let numberCups = Number(localStorage.getItem("numberCups"))
        if (numberCups === 0)
            numberCups = 1
        this.state = {
            numberCups: numberCups,
            showDescription: false,
            skipDescription: skip,
        };
        this.handleChange = this.handleChange.bind(this)
        this.increaseCups = this.increaseCups.bind(this)
        this.decreaseCups = this.decreaseCups.bind(this)
        this.showDescription = this.showDescription.bind(this)
        this.startGame = this.startGame.bind(this)
        this.goBack = this.goBack.bind(this)
    }

    handleChange(number) {

        if (number > 0 && number < 8) {
            this.setState({
                numberCups: number,
                showDescription: true
            }
            )
        }


    }

    increaseCups() {

        if (this.state.numberCups < 8) {
            const newNumber = this.state.numberCups + 1;
            console.log("increase clicked", newNumber)
            this.setState({
                numberCups: newNumber
            })
        }
    }
    decreaseCups() {
        if (this.state.numberCups > 1) {
            const newNumber = this.state.numberCups - 1;
            this.setState({
                numberCups: newNumber
            })
        }
    }

    showDescription(value) {
        console.log("handling click", this.state.skipDescription)
        if (!this.state.skipDescription) {
            this.setState({
                showDescription: true
            })
        }

    }

    goBack() {
        this.setState({
            showDescription: false
        })
    }

    startGame() {
        console.log("state number", this.state.numberCups)
        localStorage.setItem("numberCups", this.state.numberCups)
    }


    render() {
        return (
            <div>
                {((!this.state.showDescription) && (!this.state.skipDescription)) &&
                    <div>
                        <CupSelector number={this.state.numberCups} onChange={this.handleChange} increase={this.increaseCups} decrease={this.decreaseCups} />
                        <Row>
                        <button onClick={this.showDescription}>Start</button>
                        </Row>
                    </div>
                }
                {this.state.showDescription &&
                    <div>
                        <Row>
                        <Description numberCups={this.state.numberCups} goBack={this.goBack} />
                        </Row>
                        <Row>
                        <CoffeeLink to={{ pathname: '/game' }}><button onClick={this.startGame}>Start</button></CoffeeLink>
                        </Row>
                    </div>
                }
                {(this.state.skipDescription && !this.state.showDescription) &&
                    <div>
                        <CupSelector number={this.state.numberCups}
                            onChange={this.handleChange} increase={this.increaseCups}
                            decrease={this.decreaseCups} />
                        <Row>
                            <CoffeeLink to={{ pathname: '/game' }}><button onClick={this.startGame}>Start</button></CoffeeLink>
                        </Row>
                    </div>
                }
            </div>
        )
    }
}