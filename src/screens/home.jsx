import React from "react";
import styled from "styled-components";

const StyledText = styled.p`
  color: darkgoldenrod;
  text-decoration: underline;
`

export class HomeScreen extends React.Component {
    render() {
        return (
            <div>
                <h1>React parcel starter</h1>
                <StyledText>This text is styled with styled components :)</StyledText>
            </div>
        )
    }
}