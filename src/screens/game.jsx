import React from "react";
import { Cup } from "../components/cup";
import {Row, CoffeeLink, CoffeeCup, Header} from "../components/styling";
import { Link } from "react-router-dom";

export class GameScreen extends React.Component {



    constructor(props) {
        super(props)

        this.state = {
            numberCups: Number(localStorage.getItem("numberCups")),
            numberGameOver: 0,
            isPaused: false,
            time: 0,
            isOn: false,
            start: 0,
            highscore: Number(localStorage.getItem("highscore"))
        }
        this.initialState = this.state
        this.pauseGame = this.pauseGame.bind(this)
        this.gameOver = this.gameOver.bind(this)
        this.reset = this.reset.bind(this)
        this.childs = []

        this.startTimer = this.startTimer.bind(this)
        this.stopTimer = this.stopTimer.bind(this)
        this.resetTimer = this.resetTimer.bind(this)
    }

    componentDidMount() {
        this.startTimer()
    }

    renderCups() {
        let rows = []

        let numberRows = Math.ceil(this.state.numberCups / 4)
        let numberCups = this.state.numberCups
        let count = 0
        for (let i = 0; i < numberRows; i++) {

            let cups = []

            for (let j = 0; j < ((numberCups < 4) ? numberCups : 4); j++) {
                let cup = <Cup className="cup" ref={instance => {
                    if (instance) { this.childs[count++] = instance; }
                }} key={"cup-" + j} isPaused={this.state.isPaused} gameOver={this.gameOver} />
                cups.push(cup)


            }
            rows.push(<Row key={"row-" + i}>{cups}</Row>)
            if (numberCups > 4)
                numberCups = this.state.numberCups - 4
        }


        return (
            rows
        )
    }

    gameOver() {


        const gameOverCount = this.state.numberGameOver + 1

        this.setState({
            numberGameOver: gameOverCount,
        })

        if (this.state.numberCups === this.state.numberGameOver) {
            if (this.state.time > Number(localStorage.getItem("highscore"))){
                localStorage.setItem("highscore", this.state.time)
                this.setState({
                    highscore: Number(localStorage.getItem("highscore")),
                })
            }
                
            this.stopTimer()
            this.resetTimer()
           
        }



    }

    pauseGame() {

        if (!this.state.isPaused) {
            this.stopTimer()
        } else {
            this.startTimer()
        }

        this.setState({
            isPaused: !this.state.isPaused
        })






    }

    reset() {
        console.log(this.childs)
        console.log("calling rety", this.initialState)
        this.setState(
            this.initialState
        )

        this.startTimer()

        for (let child of this.childs) {
            child.reset()
        }


        /*
           for(let child in this.childs){
               child.reset()
           }
        */
    }

    startTimer() {
        this.setState({
            isOn: true,
            time: this.state.time,
            start: Date.now() - this.state.time
        })
        this.timer = setInterval(() => this.setState({
            time: Date.now() - this.state.start
        }), 1);
    }

    stopTimer() {
        this.setState({ isOn: false })
        clearInterval(this.timer)
    }

    resetTimer() {
        this.setState({ time: 0, isOn: false })
    }

    render() {
        return (
            <div>
                <Row>
                <span>Current highcore {(this.state.highscore / 1000).toFixed(2)}</span><span> s</span>
                </Row>
                {this.renderCups()}
                {(this.state.numberCups === this.state.numberGameOver) &&
                    <div>
                        <Row>
                            <label>Game Over dude</label>
                        </Row>
                        <Row>
                            <button onClick={this.reset}>Retry</button>
                        </Row>
                        <Row>
                            <CoffeeLink to="/settings"><button>Change Settings</button></CoffeeLink>
                        </Row>
                    </div>
                }
                {(this.state.isPaused) &&
                    <Row>
                        <label>Game paused</label>
                    </Row>
                }
                {(this.state.numberCups !== this.state.numberGameOver) &&
                    <div>
                        <Row>
                            <span>
                                {(this.state.time / 1000).toFixed(2)}
                        </span><span> s</span>
                    </Row>
                        <Row>
                                <button onClick={this.pauseGame}>{(!this.state.isPaused) ? "Pause" : "Resume"}</button>
                        </Row>
                    </div>
                }

            </div>
        )
    }
}