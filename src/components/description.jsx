import React from "react";

import {Row, CoffeeDescripton} from "./styling";

export class Description extends React.Component {

    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this)
        this.goBack = this.goBack.bind(this)
    }

    handleClick(e) {
        const skip = e.target.checked
        console.log("settying", skip)
        localStorage.setItem("skipDescription", skip)
    }
    goBack() {
        this.props.goBack()
    }
    render() {
        return (
            <CoffeeDescripton>
                <Row>

                    <h1>How to play</h1>
                </Row>
                <Row>

                    <p>The cups will fill from time to time. You can drink
                        them empty by clicking on a cup. Try to drink all cups
                        before they get too full. The speed of the filling process will increase after time.</p>
                </Row>
                <Row>
                    <input type="checkbox" onClick={this.handleClick} />
                    <label>Don't show this message again</label>
                </Row>
                <Row>
                    <button onClick={this.goBack}>Back</button>
                </Row>
            </CoffeeDescripton>
        )
    }
}