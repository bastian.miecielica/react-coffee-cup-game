import React from "react";

export class Cup extends React.Component {



    constructor(props) {
        super(props)
        this.state = {
            src: require('../images/cup_fill_1.png'),
            count: 0,
            gameover: false,
            fillSpeed: ((Math.random() * 2000) + 2000),
        }
        this.resetCup = this.resetCup.bind(this)
        this.startFilling = this.startFilling.bind(this)
        this.reset = this.reset.bind(this)


    }
    componentDidMount() {
        this.startFilling()
    }


    resetCup(e) {
        if (!this.state.gameover) {
            e.target.src = require('../images/cup_empty.png')
            this.setState({
                count: 0
            })
        }
    }

    reset() {
        this.setState({
            src: require('../images/cup_empty.png'),
            count: 0
        })
        this.startFilling()
    }

    startFilling() {

        //const timer = setInterval(()=>this.fillCup(), this.state.fillSpeed);
        const timer = setInterval(() => this.fillCup(), this.state.fillSpeed);

        this.setState({
            timerId: timer
        })
    }


    fillCup() {
        if (!this.props.isPaused) {
            if (this.state.fillSpeed > 100) {
                const fillSpeed = this.state.fillSpeed - 100
                this.setState({
                    fillSpeed: fillSpeed
                })
                clearInterval(this.state.timerId)
                this.startFilling()
            }
            console.log(this.state.fillSpeed)
            switch (this.state.count) {
                case 0:
                    this.setState({
                        src: require('../images/cup_fill_1.png'),
                        count: 1
                    })
                    break;
                case 1:
                    this.setState({
                        src: require('../images/cup_fill_2.png'),
                        count: 2
                    })
                    break;
                case 2:
                    this.setState({
                        src: require('../images/cup_fill_3.png'),
                        count: 3
                    })
                    break;
                case 3:
                    this.setState({
                        src: require('../images/cup_full.png'),
                        count: 4
                    })
                    break;
                default:
                    clearInterval(this.state.timerId)
                    this.setState({
                        src: require('../images/cup_too_full.png'),
                        gameover: true
                    })
                    this.props.gameOver()
            }
        }

    }



    render() {
        return (
            <button className="cupButton"><img src={this.state.src} onClick={this.resetCup} /></button>
         )
    }


}