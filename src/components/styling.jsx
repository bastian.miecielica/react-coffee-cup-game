import styled from 'styled-components';
import { Link } from 'react-router-dom'
import { Cup } from './cup';

const coffeeButton = `
background:saddlebrown;
border:none;
color:white;
font-size:20px;
font-weight:bold;
`

export const Row = styled.div`
width: 100 %;
display: inline - block;
text-align:center;
margin-top:10px;
margin-bottom:10px;
button{
${coffeeButton}
}
.cupButton{
  background:none;
  img{
    width:70px;
  }
}
span{
  color:rosybrown;
  font-weight:bold;
  font-size:18px;
}


`;

export const SelectorColumn = styled.div`
 width:49%;
 position:relative;
 height:50px;
 input{
   position:absolute;
   right:0;
 }

`;


export const Header = styled.div`
background:saddlebrown;
color:white;
padding:20px;
font-size:20px;
font-weight:bold;
  img{
    height:20px;
    margin-right:10px;
  }
`

export const CoffeeDescripton = styled.div`
p{
margin:20px;
color:saddlebrown;
font-size:18px;
font-weight:bold;
}

`

export const Selector = styled.div`
input{
  width:60px;
  height:60px;
  border:1px solid black
  font-size:25px;
  text-align:center
}
img{
  max-width:20px;
}
button{
  background:none;
  border:none;
}
`
export const CoffeeLink = styled(Link)`
button{
${coffeeButton}
}

`
