import React from "react";
import styled from "styled-components";
import { Row, Selector, SelectorColumn } from "./styling";

const StyledLabel = styled.label`
text-align:center;
`

const StyledInput = styled.input`
border:1px solid black;
height:100px;

`
export class CupSelector extends React.Component {


    constructor(props) {
        super(props)
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(e) {
        const number = e.target.value
        this.props.onChange(number)
    }

    render() {
        return (
            <div>
                <Row>
                    <label>Number of cups: </label>
                </Row>
          <Row>
                    <Selector>
                     
                        <input className="item1" type="text" onChange={this.handleChange} value={this.props.number}/>
                     
                        <button className="item2" onClick={this.props.increase}><img src={require('../images/arrow-up.png')} /></button>
                        <button className="item3" onClick={this.props.decrease}><img src={require('../images/arrow-down.png')} /></button>
              
                    </Selector>
          </Row>

            </div>
        )
    }

}